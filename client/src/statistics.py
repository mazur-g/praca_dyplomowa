import sys
import numpy as np

class Statistics:
	"""responsible for storing and updating statistics values, returned values are rounded to two decimal places"""
	def __init__(self, maxAmountOfSamples):
		self.amountOfSamples = 0
		self.min = sys.maxsize
		self.max = -sys.maxsize -1
		self.avg = 0
		self.values = []
		self.maxAmountOfSamples = maxAmountOfSamples

	def processNextValue(self,value):
		""" updates statistics values with passed value"""
		if not np.isnan(value):
			self.amountOfSamples += 1
			self.values.append(value)
			if self.amountOfSamples > self.maxAmountOfSamples:
				self.values = self.values[-self.maxAmountOfSamples:]
			self.min = min(self.values)
			self.max = max(self.values)
			self.avg = sum(self.values)/len(self.values)
		
	def setMaxAmountOfSamples(self, amount):
		""" sets max amount of samples to count statistics from"""
		self.maxAmountOfSamples = amount

	def getMin(self):
		"""returns min value from stored collection"""
		return round(self.min,2)

	def getMax(self):
		"""returns max value from stored collection"""
		return round(self.max,2)

	def getAvg(self):
		"""returns avg value from stored collection"""
		return round(self.avg,2)

	def clearStatistics(self):
		self.max = 0
		self.min = sys.maxsize
		self.avg = 0
		self.values = []
		self.amountOfSamples = 0