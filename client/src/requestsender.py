import json
import multiprocessing.connection

class Client:
	"""this is secondary client used for sending requests to server
	   to obtain data it sends requests in form: {'request': <request>, 'payload' : <payload>} to server"""
	def __init__(self, serverHost):
		self.serverAddress = (serverHost, 6004)

	def getFileFromServer(self,fileName):
		"""gets fileName from server"""
		requestJson = json.dumps({"command" : "send_file", "payload" : fileName})
		data = self.getResponseFromServerFor(requestJson)
		return data

	def getLastFileFromServer(self):
		"""gets last accesible file from server"""
		requestJson = json.dumps({"command" : "list_files",
								  "payload" : None})
		fileList = self.getResponseFromServerFor(requestJson)
		fileList.sort()
		fileName = "logFile." + fileList[-1]
		requestJson2 = json.dumps({"command" : "send_file", "payload" : fileName})
		data = self.getResponseFromFileServiceFor(requestJson2)
		return data

	def getFileListFromServer(self):
		"""gets list of available file from server"""
		requestJson = json.dumps({"command" : "list_files",
								  "payload" : None})
		fileList = self.getResponseFromServerFor(requestJson)
		fileList.sort()
		return fileList
	def getResponseFromServerFor(self,request):
		"""general method for sending request to server
		:returns: response from server"""
		conn = multiprocessing.connection.Client(self.serverAddress, authkey=b'somekey')
		conn.send(request)
		response = conn.recv()
		conn.close()
		return response    

