import re
import math
import copy
import time
import sys
import signal
from datetime import timedelta
import os
import multiprocessing
import queue    
from threading import Thread
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.pyplot import text
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg,NavigationToolbar2Tk
from matplotlib.figure import Figure
from multiprocessing.connection import Client
from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, DrawingArea
from matplotlib.patches import Rectangle
import matplotlib.dates as mdates
from matplotlib import rcParams
from datetime import datetime
from matplotlib import dates as mdates
import sys
import json
import numpy as np
from functools import reduce
if sys.version_info[0] < 3:
    import Tkinter as Tk
    import ttk
    from tkinter.filedialog import askopenfilename
    import tkMessageBox as messagebox
else:
    import tkinter as Tk
    from tkinter import ttk
    from tkinter.filedialog import askopenfilename
    from tkinter import messagebox

#custom modules
from mode import Mode
import offlineprocessor
import requestsender
import figurecontroller
import datamonitor
import configfileparser
import statistics


class SelectFileDialogWindow(Tk.Toplevel):
    """represents window that shows when select date button is clicked"""
    def __init__(self, parent):
        Tk.Toplevel.__init__(self,parent)
        self.transient(parent)
        self.grab_set()
        self.parent = parent
        self.protocol("WM_DELETE_WINDOW", self.cancel)
        button = Tk.Button(self, text="Confirm", command=self.confirm)
        button.pack()
        self.bind("<Escape>", self.cancel)
        self.focus_set()
        self.withdraw()
        self.listBox = Tk.Listbox(self, selectmode = Tk.MULTIPLE)
    def confirm(self, event=None):
        """executes when Confirm button is clicked and performs execution of command
        that was estabslished by setCommandOnConfirmation method"""
        self.parent.focus_set()
        indexList = list(self.listBox.curselection())
        joinedText = reduce(lambda x,y : x + "\n" + y, [self.listBox.get(index) for index in indexList])
        self.commandToExecuteOnConfirmation(joinedText)
        self.destroy()
    def cancel(self, event = None):
        """executes when Escape is pressed and destroys this window"""
        self.parent.focus_set()
        self.destroy()

    def setCommandOnConfirmation(self, command):
        """sets command to execute when Confirm button will be pressed"""
        self.commandToExecuteOnConfirmation = command

    def addToListBox(self, elem):
        """adds element to end of displayed list of filrs""" 
        self.listBox.insert("end", elem)
        self.listBox.pack()

    def show(self):
        """shows this window"""
        self.deiconify()

class StreamDisplay(object):

    def sendCommand(self):
        """sends command to arduino and handles return code"""
        commands = self.entry.get().split(' ')
        for command in commands:
            param = command.split('=')[0]
            value = command.split('=')[1]
            if param in self.commandsConfig:
                try:
                    value = str(int(value)).zfill(self.commandsConfig[param])
                    request = json.dumps({"command" : "send_command", "payload" : param+value+'\n' })
                    response = self.requestsenderClient.getResponseFromServerFor(request)
                except Exception as e:
                    print("Bad command format try again!")
                    return
            else:
                print(command  + " does not match commands parameters specified in config file")
                return
        (date, dict) = self.m.extractValues(self.lastMsg,['STATUS'])
        if 'STATUS' not in dict:
            messagebox.showinfo("Result", "STATUS not arrived from ARDUINO")
        else:
            status = dict['STATUS'][0]
            messagebox.showinfo("Result", "Command executed with STATUS equal to: %s" % status)

    def validateInputs(self):
        """checks the correctness of input values"""
        pass
    def setAnimation(self,ani):
        """sets animation object"""
        self.ani = ani
    def changeStatus(self):
        """handles event when Enable control checkbox is clicked
        hides and show appropriate widgets"""
        if self.var1.get() == 1:
            self.entry.configure(state='normal')
            self.var1.set(0)
            self.sendButton.pack()
        else:
            self.entry.configure(state='disabled')
            self.sendButton.pack_forget()
            self.var1.set(1)

    def setUpConnection(self, serverHost):
        """sets up connection with server"""
        address = (serverHost, 6003)
        try:
            self.conn = Client(address, authkey=b'somekey')
            self.delay = self.conn.recv()
            self.startOnlineMode = True
            if self.delay == None:
                self.startOnlineMode = False
        except ConnectionRefusedError as e:
            print("Unable to connect to server, exiting...")
            sys.exit(1)

    def quit(self):
        print("Exiting...")
        self.root.destroy()
        os.kill(os.getpid(), signal.SIGKILL)        


    def _initWindowsAndFrames(self):
        self.root = Tk.Tk()
        self.root.title("System monitoring")
        self.root.protocol("WM_DELETE_WINDOW", self.quit)
        self.tabbedWindow = ttk.Notebook(self.root)
        self.onlineModeWindow = Tk.Frame(self.tabbedWindow, name = "tabOnline")
        self.offlineModeWindow = Tk.Frame(self.tabbedWindow, name = "tabOffline")

        if self.startOnlineMode == True:
            self.frame = Tk.Frame(self.onlineModeWindow)
            self.entry = Tk.Entry(self.frame,state='disabled')
            self.canvas = FigureCanvasTkAgg(self.figureController.getFigure(Mode.ONLINE), master=self.onlineModeWindow)
            self.var1 = Tk.IntVar()
            self.var1.set(1)
            self.checkButtonEnableControl = Tk.Checkbutton(self.frame, variable=self.var1, text="Enable control", command=self.changeStatus)
            self.sendButton = Tk.Button(self.frame, text="Send command", command = self.sendCommand)
            self.canvas.get_tk_widget().pack(fill = Tk.BOTH,expand=2)
            self.canvas._tkcanvas.pack(fill=Tk.BOTH, expand=2)
            self.checkButtonEnableControl.pack(expand=1)
            self.entry.pack(expand=1)
            self.sendButton.pack(expand=1)
            self.sendButton.pack_forget()
            self.frame.pack(fill = Tk.BOTH,expand=1)
            self.onlineModeWindow.pack(fill = Tk.BOTH, expand=1)
            self.tabbedWindow.add(self.onlineModeWindow, text = "Online Mode")


        self.canvasOffline = FigureCanvasTkAgg(self.figureController.getFigure(Mode.OFFLINE), master = self.offlineModeWindow)
        self.frameOfflineModeProperties = Tk.Frame(self.offlineModeWindow)
        
        frameForToolbar = Tk.Frame(self.frameOfflineModeProperties)
        frameForToolbar.pack(expand=True, side=Tk.LEFT)
        toolbar = NavigationToolbar2Tk(self.canvasOffline, frameForToolbar)
        toolbar.update()

        self.canvasOffline.get_tk_widget().pack(fill=Tk.BOTH, expand=1)
        self.canvasOffline._tkcanvas.pack(fill = Tk.BOTH, expand=1)
        self._initializeOfflineModeSelectArea()
        self.frameOfflineModeProperties.pack(fill = Tk.BOTH,expand=1)
        self.offlineModeWindow.pack()

        self.tabbedWindow.add(self.offlineModeWindow, text = "Offline Mode")
        self.tabbedWindow.pack(fill=Tk.BOTH, expand=1)
        
    def __init__(self, paramNames, paramsToPlotOnOne):
        self.configParser = configfileparser.ConfigFileParser()
        self.serverHost = self.configParser.getServerHost()
        self.setUpConnection(self.serverHost)
        self.requestsenderClient = requestsender.Client(self.serverHost)
        self.commandsConfig = self.configParser.getCommandsConfig()
        self.figureController = figurecontroller.FigureController()
        self.data = {}
        self.dataOffline = {}
        self.paramMapping = {}
        if paramNames == None:
            self.paramNames = self.configParser.getParameterNames()
        else:
            self.paramNames = paramNames
        if paramsToPlotOnOne == None:
            self.paramsToPlotOnOne = []
            self.paramsToPlotOnSingle = self.paramNames
        else:
            self.paramsToPlotOnOne = paramsToPlotOnOne
            self.paramsToPlotOnSingle = [param for param in self.paramNames for paramList in self.paramsToPlotOnOne if param not in paramList.split(':')] 
        self.paramsConfig = self.configParser.getParamsConfig()
        multiplierDict = {'s':1, 'm':60, 'h':3600}
        for pos, paramName in enumerate(self.paramNames):
            if self.startOnlineMode == True:
                multiplier = multiplierDict[self.paramsConfig[paramName]['unit']]
                amount = (int)(1/self.delay*self.paramsConfig[paramName]['period']*multiplier) 
            else:
                amount = None
            self.data[paramName] = {"time"              : [],
                                    "axes"              : None, 
                                    "values"            : [],
                                    "amount"            : amount,
                                    "counter"           : 0,
                                    "countDown"         : 0,
                                    "statistics"        : statistics.Statistics(amount),
                                    "markers"           : [],
                                    "enable_markers"    : self.paramsConfig[paramName]['enable_markers'],
                                    "enable_statistics" : self.paramsConfig[paramName]['enable_statistics'],
                                    "enable_control"    : self.paramsConfig[paramName]['enable_control'],
                                    "is_initialized"    : False,
                                    "is_decided"        : False,
                                    "number_of_good_values" : 0}

            self.paramMapping[paramName] = pos
        self.offlineProcessor = offlineprocessor.OfflineProcessor(self.paramNames, self.figureController)
        self._initSubPlots()
        self._initWindowsAndFrames()
        self.queue = queue.Queue()
        self.m = datamonitor.DataMonitor(self.queue, paramNamesToExtract = self.paramNames)
        self.initializedPlots = []
        self.initializedParams = []

    def _initSubPlots(self):
        if self.startOnlineMode == True:
            self.figureController.createNewFigure(Mode.ONLINE, figsize=(8.,6))
            self.figureController.setPlotDimensions(Mode.ONLINE, len(self.paramsToPlotOnSingle) + len(self.paramsToPlotOnOne))
            self.figureController.initializeSubPlotsForFigure(Mode.ONLINE, self.paramsToPlotOnSingle,self.paramsToPlotOnOne, self.paramsConfig)  

        self.figureController.createNewFigure(Mode.OFFLINE, figsize=(8,6))
        self.figureController.setPlotDimensions(Mode.OFFLINE, len(self.paramsToPlotOnSingle) + len(self.paramsToPlotOnOne))
        self.figureController.initializeSubPlotsForFigure(Mode.OFFLINE, self.paramsToPlotOnSingle,self.paramsToPlotOnOne, self.paramsConfig)

    def clearFiguresInOfflineModeButtonHandler(self):
        """executes when Clear button is clicked, takes listOfParams from Entry and clears the plots for specified params"""
        listOfParams = self.entryParamList.get().split(';')
        if listOfParams != ['']:
            self.offlineProcessor.clearAxesFor(Mode.OFFLINE, listOfParams)
        else:
            self.offlineProcessor.clearAxesFor(Mode.OFFLINE, None)

    def openSelectFileDialog(self):
        """executes when Select date button is clicked and shows dialog to select dates"""
        self.selectFileWindow = SelectFileDialogWindow(self.offlineModeWindow)
        fileList = self.requestsenderClient.getFileListFromServer()
        [self.selectFileWindow.addToListBox(elem ) for elem in fileList]
        self.selectFileWindow.setCommandOnConfirmation(lambda txt: self.labelSelectedDate.config(text=txt))
        self.selectFileWindow.show()

    def plotInOfflineModeButtonHandler(self):
        """executes when Draw button is clicked, takes list of param from Entry and selected date nextly takes a file from server and plot graphs"""
        self.validateInputs()
        logfileSuffix = 'logFile.'
        listOfParams = self.entryParamList.get().split(';')
        self.figureController.clearAxesFor(Mode.OFFLINE, listOfParams)
        for file in self.labelSelectedDate["text"].split('\n'):
            fileFrom = logfileSuffix + file
            self.offlineProcessor.prepareContentFromFile(listOfParams, fileFrom)
            self.offlineProcessor.plotForParams(listOfParams)
        self.canvasOffline.draw()

    def _initializeOfflineModeSelectArea(self):
        self.labelDFrom= Tk.Label(self.frameOfflineModeProperties, text="Date from: ")
        self.labelDFrom.pack(expand=True, side=Tk.LEFT)
        self.labelSelectedDate = Tk.Label(self.frameOfflineModeProperties, text = "None")
        self.labelSelectedDate.pack(expand=True, side=Tk.LEFT)
        buttonSelectFile = Tk.Button(self.frameOfflineModeProperties, text = "Select date", command = self.openSelectFileDialog)
        buttonSelectFile.pack(expand=True, side=Tk.LEFT)
        labelListOfParams = Tk.Label(self.frameOfflineModeProperties, text = "Parameter list")
        labelListOfParams.pack(expand=True, side=Tk.LEFT)
        self.entryParamList = Tk.Entry(self.frameOfflineModeProperties)
        self.entryParamList.pack(expand=True, side=Tk.LEFT)
        self.plotInOfflineModeButton = Tk.Button(self.frameOfflineModeProperties, text = "Draw" , command = self.plotInOfflineModeButtonHandler)
        self.plotInOfflineModeButton.pack(expand=True, side=Tk.LEFT)
        self.clearPlotsButton = Tk.Button(self.frameOfflineModeProperties,  text="Clear", command = self.clearFiguresInOfflineModeButtonHandler)
        self.clearPlotsButton.pack(expand=True, side=Tk.LEFT)

    def listen(self):
        """listens for new data record from server"""
        while True:
            try:
                msg = self.conn.recv()
                self.queue.put(msg)
                self.lastMsg = msg
            except Exception as e:
                print(e)
                sys.exit(1)

    def _init_plot(self):
        self.lines = []
        for param, line in self.figureController.plot_lines_gen(Mode.ONLINE):
            self.lines.append(line)
            self.data[param]["line"] = line

    def degradeMarkerIndexes(self, paramName):
        """moves indexes of markers to be compatible with data flow(online mode) for specified param"""
        if self.data[paramName]["counter"] > self.data[paramName]["amount"]:
            self.figureController.degradeMarkerIndexes(Mode.ONLINE,paramName)

    def addNewMarker(self, paramName, marker):
        """adds new marker for specified param"""
        self.figureController.addNewMarker(Mode.ONLINE, paramName, marker)

    def processDataFromGenerator(self, date, paramName, value, isGood):
        """
        processes unpacked data from update method, in accordance with configuration controls period of displayed values, handles statistics and markers upgrade

        :param date: date when value was registered
        :param paramName: name of param
        :param value: value for param
        :param isGood: indicates if value places between limits

        :returns: tuple - (list, params) which indicates which lines and params should be updated 

        """
        paramDict = self.data[paramName]
        paramDict["time"].append(date)
        paramDict["values"].append(value)
        if not np.isnan(value):
            paramDict["number_of_good_values"] +=1
        if paramDict["number_of_good_values"] > 1:
            paramDict['is_initialized'] = True
        if paramDict['is_initialized'] == True and paramDict['is_decided'] == False:
            self.initializedPlots.append(paramDict['line'])
            self.initializedParams.append(paramName)
            paramDict['is_decided'] = True

        if len(self.lines) == len(self.initializedPlots):
            self.processDataFromGenerator = self._processDataFromGeneratorWithoutChecking

        paramDict["counter"] += 1
        if paramDict["counter"] > paramDict["amount"]:
            paramDict["values"] = paramDict["values"][1:]
            paramDict["time"] = paramDict["time"][1:]
        paramDict["line"].set_xdata(paramDict["time"])
        paramDict["line"].set_ydata(paramDict["values"])
        if paramDict["enable_markers"] == True:
            self.degradeMarkerIndexes(paramName)
        self.controlValue(paramName, value, isGood)
        if paramDict['enable_statistics'] and paramDict['is_initialized']:
            statistics = self.data[paramName]["statistics"]
            statistics.processNextValue(value)
            minV = statistics.getMin()
            maxV = statistics.getMax()
            avgV = statistics.getAvg()
            values = "min: {}\nmax: {}\navg: {}".format(minV,maxV,avgV)
            self.figureController.setStatisticsValuesFor(Mode.ONLINE, paramName ,values)
        return (self.initializedPlots, self.initializedParams)

    def _processDataFromGeneratorWithoutChecking(self, date, paramName, value, isGood):
        paramDict = self.data[paramName]
        paramDict["time"].append(date)
        paramDict["values"].append(value)
        paramDict["counter"] += 1
        if paramDict["counter"] > paramDict["amount"]:
            paramDict["values"] = paramDict["values"][1:]
            paramDict["time"] = paramDict["time"][1:]
        paramDict["line"].set_xdata(paramDict["time"])
        paramDict["line"].set_ydata(paramDict["values"])
        if paramDict["enable_markers"] == True:
            self.degradeMarkerIndexes(paramName)
        self.controlValue(paramName, value, isGood)
        if paramDict['enable_statistics']:
            statistics = paramDict["statistics"]
            statistics.processNextValue(value)
            minV = statistics.getMin()
            maxV = statistics.getMax()
            avgV = statistics.getAvg()
            values = "min: {}\nmax: {}\navg: {}".format(minV,maxV,avgV)
            self.figureController.setStatisticsValuesFor(Mode.ONLINE, paramName ,values)
        return (self.lines, None)

    def update(self,dataTuple): 
        """unpacks values that came from datamonitor generator and pass them to processDataFromGenerator
        after processing refreshes appropriate params plots

        :param dataTuple: tuple with data in format (date, {key1 :(value1,isGood1), key2: (value2,isGood2)})

        :returns: list -- lines that will be used by animation.FuncAnimation object
        """
        date, data = dataTuple
        lines = []
        params = None
        for paramName, (value, isGood) in data.items():
            (lines, params) = self.processDataFromGenerator(date, paramName, value, isGood)
        self.figureController.refreshAxesFor(Mode.ONLINE, params)
        return lines

    def controlValue(self,param, value, isGood):      
        """used to indicate status of control element and to insert new marker if value has passed the limits"""
        paramDict = self.data[param]
        if not isGood:
            index = paramDict['values'].index(value)
            paramDict['countDown']=0
            if paramDict['enable_control'] == True:
                self.figureController.setThresholdExceededFor(Mode.ONLINE, param)
            if paramDict['enable_markers'] == True:
                self.addNewMarker(param, index)
        else:
            paramDict['countDown']+=1
            if paramDict['countDown'] >= paramDict["amount"] and paramDict['enable_control']:
                self.figureController.returnToAcceptableStatusFor(Mode.ONLINE, param)

    def start(self):
        """
        starts the application, for this purpose starts listening for new data record from server in new thread and initializes animation.FuncAnimation object
        """
        if self.startOnlineMode == True:
            thread = Thread(target = self.listen)
            thread.start()

           # sd.set_lims((0,100),(90,140))
            self._init_plot()

            ani = animation.FuncAnimation(self.figureController.getFigure(Mode.ONLINE), self.update, self.m.generateNewData, blit=True, interval=self.delay*500)
            self.setAnimation(ani)
        Tk.mainloop()
