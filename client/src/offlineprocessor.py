import sys
import matplotlib.pyplot as plt
import matplotlib as mpl
from functools import reduce

#custom modules
import datamonitor
from mode import Mode
import requestsender
import statistics
import configfileparser
import figurecontroller

class OfflineProcessor:
	"""responsible for taking actions in offline mode"""
	def __init__(self, paramNames, figureController):
		self.data = {}
		configParser = configfileparser.ConfigFileParser()
		paramsConfig = configParser.getParamsConfig()
		serverHost = configParser.getServerHost()
		for paramName in paramNames:
			self.data[paramName] = {"time"           : [],
									"values"         : [],
									"statistics"     : statistics.Statistics(sys.maxsize),
									"markers"        : [],
                                   	"enable_markers"    : paramsConfig[paramName]['enable_markers'],
                                    "enable_statistics" : paramsConfig[paramName]['enable_statistics'],
                                    "enable_control"    : paramsConfig[paramName]['enable_control']
									}
		self.figureController  = figureController
		self.requestsenderClient = requestsender.Client(serverHost)
		self.dataMonitor 	   = datamonitor.DataMonitor(paramNamesToExtract = paramNames)

	def clearAxesFor(self,figName, paramNames):
		"""clears axes for specified figure and list of params in it"""
		self.figureController.clearAxesFor(figName,paramNames)

	def listAvailableFiles(self, list_size):
		"""sends request to server and returns list of available files"""
		fileList = self.requestsenderClient.getFileListFromServer()
		if list_size != None:
			fileList = fileList[-list_size:]
		return reduce(lambda x,y : x + '\n' + y, fileList)

	def prepareContentFromFile(self, paramList, fileFrom=None):
		"""extracts data from file processes them and prepares data for plotting graphs"""
		self.clearDataForParams(paramList)
		if fileFrom == None:
			lines = self.requestsenderClient.getLastFileFromServer()
		else:
			lines = self.requestsenderClient.getFileFromServer(fileFrom)


		for line in lines:
			date, data = self.dataMonitor.extractValues(line.rstrip(), paramList)
			for paramName, (value, isGood) in data.items():
				self.data[paramName]["time"].append(date)
				self.data[paramName]["values"].append(value)
				self.data[paramName]["statistics"].processNextValue(value)
				if not isGood and self.data[paramName]["enable_control"]:
					self.figureController.setThresholdExceededFor(Mode.OFFLINE, paramName)
					if self.data[paramName]["enable_markers"] == True:
						index = self.data[paramName]["values"].index(value)
						self.figureController.addNewMarker(Mode.OFFLINE, paramName, index)

		for paramName in paramList:
			if self.data[paramName]["enable_statistics"] == True:
				statistics = self.data[paramName]["statistics"]
				minV = statistics.getMin()
				maxV = statistics.getMax()
				avgV = statistics.getAvg()
				values = "min: {}\nmax: {}\navg: {}".format(minV,maxV,avgV)
				self.figureController.setStatisticsValuesFor(Mode.OFFLINE, paramName ,values)
					
	def plotForParams(self, listOfParams):
		"""plots graphs for specified params"""
		for paramName in listOfParams:
			xData = self.data[paramName]["time"]
			yData = self.data[paramName]["values"]
			self.figureController.plotForParam(Mode.OFFLINE, paramName, xData, yData)

	def clearDataForParams(self, paramNames):
		"""deletes stored data such as data for x-axis, y-axis and statistics for specified params"""
		for paramName in paramNames:
			self.data[paramName]["time"] = []
			self.data[paramName]["values"] = []
			self.data[paramName]["statistics"].clearStatistics()

	def drawRaw(self, paramList, file=None, pdfPath=None):
		"""
		draws and show figure in raw mode ie without starting client process

		:param file: fileName to plot from
		:param pdfPath: path where figure will be saved as pdf for example ~/plot1 
		"""
		if file!=None:
			file = "logFile." + file
		figure = self.figureController.getFigure(Mode.OFFLINE)
		self.prepareContentFromFile(paramList, file)
		self.plotForParams(paramList)
		if pdfPath != None:
			figure.savefig(pdfPath + '.pdf', bbox_inches="tight")
			sys.exit(0)
		plt.show()




