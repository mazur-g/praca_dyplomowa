from datetime import datetime
import re
import os
import signal
import time
import matplotlib.dates as mdates
import sys
import numpy as np
import difflib
#custom modules
import configfileparser


class DataMonitor(object):
    """responsible for monitor data that are in queue passed in __init__ method"""
    def __init__(self,queue=None,**kwargs):
        try:
            configParser = configfileparser.ConfigFileParser()
            paramsConfig = configParser.getParamsConfig()
            self.paramNames = configParser.getParameterNames()
            self.paramNamesToExtract = kwargs['paramNamesToExtract'] 
            self.valuesIndicator = configParser.getValuesIndicator()
            self.data = {}
            self.paramMapping = {}
            self.bufferedData = []
            for pos, paramName in enumerate(self.paramNames):
                self.data[paramName] = {}
                self.data[paramName]['min'] = paramsConfig[paramName]['min_value']
                self.data[paramName]['max'] = paramsConfig[paramName]['max_value'] 
                self.paramMapping[pos] = paramName # for example: {0:'T',1:'V'}
            self.daSign = configParser.getDASign()
        except configfileparser.ConfigFileNotFound:
            print("Config file not found. Exiting...")
            os.kill(os.getpid(), signal.SIGKILL)
        except Exception as e:
            print(e)
            os.kill(os.getpid(), signal.SIGKILL)

        self.queue = queue


    def extractValues(self, line, paramNames=None):
        """extracts values from single data record from queue
        extracted values are in format: (date, {key1 :(value1,isGood1), key2: (value2,isGood2)})

        :param line: data record
        :param paramNames: list of params that should be extracted from record if not passed then params to extract are taken from params passed in __init__ 
        """
        if paramNames == None:
            paramNamesToExtract = self.paramNamesToExtract
        else:
            paramNamesToExtract = paramNames
        if self.valuesIndicator == 'key_value':
            tmp = ""
            for pn in self.paramNames[:-1]:
                tmp = tmp + pn + "|"
            tmp = tmp + self.paramNames[-1]
            regex = r"^([0-9]{4}-(?:0|1)[0-9]-(?:0|1|2|3)[0-9] [0-9]{2}:[0-9]{2}:[0-9]{2}(?:\.[0-9]+)?)((?:\t(?:%s)%s(NaN|-?[0-9]+\.?[0-9]*))+)$" %(tmp, self.daSign)
            pattern = re.compile(regex)
            match = pattern.match(line)
            if not match:
                print("Invalid format of message. Exiting...")
                os.kill(os.getpid(), signal.SIGKILL)
            else:
                date = match.group(1)
                if len(date) <= 19:
                    date = datetime.strptime(date,"%Y-%m-%d %H:%M:%S")
                else:
                    date = datetime.strptime(date,"%Y-%m-%d %H:%M:%S.%f")
                date = mdates.date2num(date)
                valueDict = {}
                pattern2 = re.compile(r"\t(%s)%s(-?[0-9]+\.?[0-9]*|NaN)" % (tmp, self.daSign))
                for (param, value) in re.findall(pattern2, match.group(2)):
                    if paramNamesToExtract != None:
                        if param not in paramNamesToExtract:
                            continue
                    if value=='NaN':
                        value = np.nan
                    else:
                        value = float(value)
                    valueDict[param] = (value, self.controlValue(param, value))
                return (date, valueDict)
        elif self.valuesIndicator == 'positional':
            regex = r"^([0-9]{4}-(?:0|1)[0-9]-(?:0|1|2|3)[0-9] [0-9]{2}:[0-9]{2}:[0-9]{2}(?:\.[0-9]+)?)((%(0)s(NaN|-?[0-9]+\.?[0-9]*))+)$" % {'0' : self.daSign}
            pattern = re.compile(regex)
            match = pattern.match(line)
            if not match:
                print("Invalid format of message. Exiting...")
                os.kill(os.getpid(), signal.SIGKILL)

            date = match.group(1)
            if len(date) <= 19:
                date = datetime.strptime(date,"%Y-%m-%d %H:%M:%S")
            else:
                date = datetime.strptime(date,"%Y-%m-%d %H:%M:%S.%f")
            date = mdates.date2num(date)
            valueDict = {}
            values = match.group(2).split()
            for pos, value in enumerate(values):
                if paramNamesToExtract != None:
                    if(self.paramMapping[pos] not in paramNamesToExtract):
                        continue
                if value=='NaN':
                    value = np.nan
                else:
                    value = float(value)
                valueDict[self.paramMapping[pos]]=(value, self.controlValue(self.paramMapping[pos],value))
            return (date, valueDict)

    def controlValue(self,paramName,value):
        """ checks if value does not overstep limits
        :returns: True or False"""
        if not np.isnan(value):
            if value > self.data[paramName]['max'] or value < self.data[paramName]['min']:
                return False
        return True

    def generateNewData(self):
        """constantly waits for records in queue,  if record appears then record is parsed by extractValues method and parsed data is returned """
        while True:
            if not self.queue.empty():
                line = self.queue.get_nowait()
                (date, valueDict) = self.extractValues(line)    
                yield (date, valueDict)
            else:
                continue