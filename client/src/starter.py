import argparse
import sys

#custom modules
import graph
import offlineprocessor
import configfileparser
import figurecontroller
from mode import Mode


class AppStarter:
	def __init__(self):
		self.parser = parser = argparse.ArgumentParser(description="Starts the system monitor with the options specified below", prog="python starter.py")
		parser.add_argument("-p", "--params", help = "specifies parameters to plot", nargs='+', metavar=('PARAM1', 'PARAMN'))
		parser.add_argument("-oop", "--paramsToPlotOnOne", help = "specifies parameters that will be on one plot", metavar=('PARAMS1', 'PARAMSN'), nargs='+')
		self.subparsers = subparsers = parser.add_subparsers(dest='command')
		raw_description = "Plots graphs without GUI ie in raw mode.\nDefault it uses last accesible file and plots all params."
		parser_raw = subparsers.add_parser('raw', help="please type 'python starter.py raw -h' for description",description=raw_description) 
		parser_raw.add_argument("-p", "--params", help = "specifies parameters to plot", nargs='+', metavar=('PARAM1', 'PARAMN'))
		parser_raw.add_argument("-oop", "--paramsToPlotOnOne", help = "specifies parameters that will be on one plot", metavar=('PARAMS1', 'PARAMSN'), nargs='+')
		group = parser_raw.add_mutually_exclusive_group()
		group.add_argument("-l", "--list", help = "list available files to plot from",dest='list_size',type=int, default=-1, nargs='?')
		group.add_argument("file", help ="plot specified file", nargs='?')
		parser_raw.add_argument("-s", "--save", help ="save figure as pdf to specified path", dest="pdf_path", default=None)
		self.configParser = configfileparser.ConfigFileParser()
		
	def start(self):
		args = self.parser.parse_args()
		if args.command =='raw':
			if args.params == None:
				paramNames = self.configParser.getParameterNames()
			else:
				paramNames = args.params
			figureController = figurecontroller.FigureController()
			offlineProcessor = offlineprocessor.OfflineProcessor(paramNames, figureController)
			if args.list_size != -1:
				if args.pdf_path != None:
					print("Cannot combine -l option with -s option!")
					sys.exit(0)
				print(offlineProcessor.listAvailableFiles(args.list_size))
				sys.exit(0)
			else:
				if args.paramsToPlotOnOne == None:
					paramsToPlotOnOne = []
					paramsToPlotOnSingle = paramNames
					allParamsFromPlotOnOne = []
				else:

					paramsToPlotOnOne = args.paramsToPlotOnOne
					allParamsFromPlotOnOne = []
					for paramList in paramsToPlotOnOne:
						for param in paramList.split(':'):
							allParamsFromPlotOnOne.append(param)
					paramsToPlotOnSingle = [param for param in paramNames if param not in allParamsFromPlotOnOne]

				allParams = paramsToPlotOnSingle + allParamsFromPlotOnOne

				figureController.createNewFigure(Mode.OFFLINE, figsize=(8,6), toolbar='None')
				figureController.setPlotDimensions(Mode.OFFLINE, len(paramsToPlotOnSingle) + len(paramsToPlotOnOne))
				figureController.initializeSubPlotsForFigure(Mode.OFFLINE, paramsToPlotOnSingle, paramsToPlotOnOne, self.configParser.getParamsConfig())
				offlineProcessor.drawRaw(allParams, args.file, args.pdf_path)
		else:
			graph.StreamDisplay(args.params,args.paramsToPlotOnOne).start()

if __name__ == '__main__':
	appStarter = AppStarter()
	appStarter.start()
