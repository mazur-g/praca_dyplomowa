import json
import os

class ConfigFileNotFound(Exception):
	"""custom exception to be more explicit"""
	pass

class ConfigFileParser():
	"""responsible for parsing configuration file"""
	def __init__(self):
		cfgFilePath = os.path.dirname(os.path.abspath(__file__)) + '/../config/config.json'
		try:
			with open(cfgFilePath, 'r') as f:
				self.config_dict = json.load(f)
		except IOError:
			raise ConfigFileNotFound()

		self.valuesIndicator = self.config_dict["values_indicator"]
		self.DASign = self.config_dict["delimiter/assignment_sign"]
		self.serverHost = self.config_dict["server_host"]
		self.parameterNames = []
		self.paramsConfig = {}
		for paramConfig in self.config_dict["parameters"]:
			paramName = list(paramConfig.keys())[0]
			self.parameterNames.append(paramName)
			self.paramsConfig[paramName] = paramConfig[paramName]
		self.commandsConfig = {}
		for commandConfig in self.config_dict["commands"]:
			paramName = commandConfig[0]
			numberOfCharacters = commandConfig[1]
			self.commandsConfig[paramName] = numberOfCharacters

	def getCommandsConfig(self):
		"""returns commands config as dictionary"""
		return self.commandsConfig

	def getParamsConfig(self):
		"""returns params config as dictionary"""
		return self.paramsConfig

	def getValuesIndicator(self):
		"""returns values indicator"""
		return self.valuesIndicator
	def getDASign(self):
		"""returns delimiter/assignment sign"""
		return self.DASign

	def getParameterNames(self):
		"""returns parameter names as list"""
		return self.parameterNames

	def getServerHost(self):
		"""returns server host"""
		return self.serverHost

if __name__ == '__main__':
	ConfigFileParser()

