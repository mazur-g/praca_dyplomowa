from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.offsetbox import AnchoredOffsetbox, TextArea, HPacker, DrawingArea
from matplotlib.patches import Rectangle
import matplotlib.dates as mdates
import math
import numpy as np
from datetime import datetime
from mode import Mode

class FigureController:
	"""
	responsible for executing common tasks for figure
	and is able to managing couple of figures at once
	"""
	def __init__(self):
		self.holdFigures = {}	

	def __make_patch_spines_invisible(self,ax):
		ax.set_frame_on(True)
		ax.patch.set_visible(False)
		for sp in ax.spines.values():
			sp.set_visible(False)


	def createNewFigure(self, figureName, **kwargs):
		"""
		creates new figure with specified name

		:param figureName: name of newly created figure 

		kwargs is used to specify additional options to figure such as toolbar
		"""
		if 'toolbar' in kwargs:
			mpl.rcParams['toolbar'] = kwargs['toolbar'] 
		newFigure = plt.figure(figsize=kwargs['figsize'])
		self.holdFigures[figureName] = {}
		self.holdFigures[figureName]['figure'] = newFigure
		self.holdFigures[figureName]['params'] = {}

	def getFigure(self, figureName):
		"""
		returns specified figure

		:param figureName: name of figure to get
		"""
		return self.holdFigures[figureName]['figure']

	def initializeSubPlotsForFigure(self, figureName, paramNames, paramsToPlotOnOne,paramsConfig):
		"""
		initializes subplots for figure on specified figure

		:param paramNames: list of params that will not share subplot with other params
		:param paramsToPlotOnOne: list of params that will share subplot each other
		:param paramsConfig: dictionary with dictionaries of config attributes for example dictionary['T'] contains config values for parameter T  
		"""
		try:
			if figureName in self.holdFigures:
				figureDict = self.holdFigures[figureName]
				figure = figureDict['figure']
				axes = figureDict['axes'] = []
				dimensions = self.holdFigures[figureName]['dimensions']
				gpos = 1
				for pos in range(1, len(paramNames) + 1):
					paramName = paramNames[pos-1]
					ax = figure.add_subplot(dimensions[0],dimensions[1], pos)
					ax.set_title(paramName, loc = 'left')
					axes.append(ax)
					figureDict['params'][paramName]= {}
					figureDict['params'][paramName]['line_color'] = 'C0'
					figureDict['params'][paramName]['markers'] = []
					figureDict['params'][paramName]['line'] = None
					figureDict['params'][paramName]['enable_statistics'] = paramsConfig[paramName]['enable_statistics']
					figureDict['params'][paramName]['enable_markers'] = paramsConfig[paramName]['enable_markers']
					figureDict['params'][paramName]['enable_control'] = paramsConfig[paramName]['enable_control']
					FigureController.__initializePlotStatisticsBoxes(ax, paramsConfig[paramName]['enable_statistics'], paramsConfig[paramName]['enable_control'], figureDict['params'][paramName])
					gpos = pos+1


				line_colors = { 0 : 'C0', 1 : 'green', 2: 'orange' }

				for params in paramsToPlotOnOne:
					paramList = params.split(':')
					position = 0
					for param in paramList:
						if position == 0:
							ax = axRef = figure.add_subplot(dimensions[0],dimensions[1], gpos)
						else:
							ax = axRef.twinx()
						ax.set_ylabel(param)
						if position == 2:
							self.__make_patch_spines_invisible(ax)
							ax.spines["right"].set_position(("axes", 1.13))

						figureDict['params'][param] = {}
						figureDict['params'][param]['markers'] = []
						figureDict['params'][param]['line'] = None
						figureDict['params'][param]['enable_statistics'] = paramsConfig[param]['enable_statistics']
						figureDict['params'][param]['enable_markers'] = paramsConfig[param]['enable_markers']
						figureDict['params'][param]['enable_control'] = paramsConfig[param]['enable_control']

						plotColor = line_colors[position]
						figureDict['params'][param]['line_color'] = plotColor
						ax.tick_params(axis='y', colors=plotColor)
						ax.yaxis.label.set_color(plotColor)
						axes.append(ax)	

						FigureController.__initializePlotStatisticsBoxes(ax, paramsConfig[param]['enable_statistics'], paramsConfig[param]['enable_control'], figureDict['params'][param], True, position,len(paramList), param)
						position +=1

					gpos += 1

				for ax in axes:
					ax.xaxis_date()
					ax.format_coord = lambda x, y: ""
					ax.get_xaxis().set_major_formatter(mdates.DateFormatter('%H:%M:%S'))
					ax.ticklabel_format(axis='y',style='plain')
					for tick in ax.get_xticklabels():
						tick.set_rotation(45)
						tick.set_fontsize(10)

				figure.subplots_adjust(hspace=0.70)
				figure.subplots_adjust(wspace=0.4)
				figure.subplots_adjust(right=0.85)
		except Exception as e:
			print(e)


	def degradeMarkerIndexes(self, figureName, paramName):
		"""moves indexes of markers to be compatible with data flow(online mode) for specified param"""
		paramDict = self.holdFigures[figureName]['params'][paramName]
		line = paramDict['line']
		markers = paramDict['markers']
		new_markers = [(marker - 1) for marker in markers if marker-1 >=0]
		paramDict['markers'] = new_markers
		if figureName == Mode.ONLINE:
			line.set_markevery(new_markers)

	def addNewMarker(self, figureName, paramName, marker):
		"""adds new marker for specified param"""
		paramDict = self.holdFigures[figureName]['params'][paramName]
		line = paramDict['line']
		currMarkers = paramDict['markers']
		currMarkers.append(marker)
		if figureName == Mode.ONLINE:
			line.set_markevery(currMarkers)

	def setMarkers(self, figureName, paramName):
		"""sets markers for specified param"""
		paramDict = self.holdFigures[figureName]['params'][paramName]
		line = paramDict['line']
		line.set_markevery(paramDict['markers'])

	def plotForParam(self, figureName, paramName, xData, yData):
		"""
		plots param for specified paramName on specified figure

		:param figureName: figure to plot on
		:param paramName: param to plot for
		:param xData: list of data for x-axis
		:param yData: list of data for y-axis
		"""
		paramDict = self.holdFigures[figureName]['params'][paramName]
		axes = paramDict['axes']
		argDict = None
		if paramDict['enable_markers'] == True:
			argDict = {'color':paramDict['line_color'],'markevery':paramDict['markers'], 'marker':'.','markersize':2, 'markerfacecolor':'r', 'markeredgecolor':'r'}
		else:
			argDict = {'color':paramDict['line_color']}
		axes.plot_date(xData, yData, '-', **argDict)

		axes.relim()
		axes.autoscale()




	def refreshAxesFor(self, figureName, paramNames=None):
		"""
		refhreshes the plot for specified figure

		:param figureName: figure to refresh on
		:param paramNames: list of params that should be refreshed if this argument is not passed that all params that figure holds will be refreshed
		"""
		if paramNames == None:
			for ax in self.holdFigures[figureName]['axes']:
				ax.relim()
				ax.autoscale()
		else:
			for param in paramNames:
				self.holdFigures[figureName]['params'][param]['axes'].relim()
				self.holdFigures[figureName]['params'][param]['axes'].autoscale()
		self.holdFigures[figureName]['figure'].canvas.draw()



	def plot_lines_gen(self, figureName):
		"""
		generator
		initializing plot for figure, plots for all param that are in figureName with no data

		yields tuple (param, newlyCreatedLine)
		"""
		for param, paramDict in self.holdFigures[figureName]['params'].items():
			line, = paramDict['axes'].plot_date([],[],'-', color=paramDict['line_color'])
			paramDict['line'] = line
			line.set_markerfacecolor('r')
			line.set_markeredgecolor('r')
			line.set_marker('.')
			line.set_markersize(2)
			line.set_markevery([])
			yield param, line

	def clearAxesFor(self, figName, paramNames = None):
		"""removes plots for specified figure

		:param figName: name of figure to remove from
		:param paramNames: list of params which plots should be removed if not passed then plots for all params will be removed"""
		if paramNames == None:
			for ax in self.holdFigures[figName]['axes']:
				ax.lines = []
				ax.relim()
		else:
			for paramName in paramNames:
				self.holdFigures[figName]['params'][paramName]['axes'].lines = []
				self.holdFigures[figName]['params'][paramName]['axes'].relim()
		self.holdFigures[figName]['figure'].canvas.draw()


	def setStatisticsValuesFor(self, figureName, paramName, values):
		"""set text for statistic box for specified figureName, paramName

		:param values: string with statistics values to show"""
		self.holdFigures[figureName]['params'][paramName]['textArea'].set_text(values)

	def setThresholdExceededFor(self, figureName, paramName):
		"""changes color of control element to red for specified figure and param"""
		self.holdFigures[figureName]['params'][paramName]['controlRect'].set_facecolor('r')

	def returnToAcceptableStatusFor(self, figureName, paramName):
		"""changes color of control element to green for specified figure and param"""
		self.holdFigures[figureName]['params'][paramName]['controlRect'].set_facecolor('g')
				
	def setPlotDimensions(self, figureName, numberOfPlots):
		"""calculates dimensions for subplots for specified figure

		:param: numberOfPlots: number of subplots that should be displayed""" 
		if figureName in self.holdFigures:
			rows = math.floor(math.sqrt(numberOfPlots))
			columns = math.ceil(math.sqrt(numberOfPlots))
			if(rows*columns) < numberOfPlots:
				if(rows >= columns):
					columns+=1
				else:
					rows+=1
			self.holdFigures[figureName]['dimensions'] = (rows, columns)

	@staticmethod
	def __initializePlotStatisticsBoxes(ax, enableStatistics, enableControl, figureParamDict, sharesPlot = False, position=None,totalParams=None, paramName = None):
		if sharesPlot == True:
			if totalParams == 3:
				posMapping = {0 : 0, 1: 0.35, 2: 0.7}
			elif totalParams == 2:
				posMapping = {0 : 0, 1: 0.6}

			boxPos = posMapping[position]
		else:
			boxPos = 0.32
		text = "min: {}\nmax: {}\navg: {}".format("none","none","none")
		box0 = TextArea(paramName, textprops=dict(color="k"))
		box1 = TextArea(text, textprops=dict(color="k"))
		box2 = DrawingArea(20, 20, 0, 0)
		rect = Rectangle((0,0), width=20,height=20, fc = 'g')
		box2.add_artist(rect)
		boxesToAdd = []
		if sharesPlot:
			boxesToAdd.append(box0)
		if enableStatistics:
			boxesToAdd.append(box1)
		if enableControl:
			boxesToAdd.append(box2)
		box = HPacker(children=boxesToAdd,
					  align='center',
					  pad=0, sep=5)
		anchored_box = AnchoredOffsetbox(
					 loc=3,
					 child=box,
					 pad=0.15,
					 frameon=True,
					 bbox_to_anchor=(boxPos, 1.02),
					 bbox_transform=ax.transAxes,
					 borderpad=0.15,
					 )
		figureParamDict['controlRect'] = rect
		figureParamDict['textArea'] = box1
		figureParamDict['axes'] = ax
		if enableStatistics or enableControl:
			ax.add_artist(anchored_box)
		



		

