from multiprocessing.connection import Client
from multiprocessing.connection import Listener
from threading import Thread
from pprint import pprint
import socket
import os
import json
import time


def unix_find(pathin):
    """Return results similar to the Unix find command run without options
    i.e. traverse a directory tree and return all the file paths
    """
    return [os.path.join(path, file)
            for (path, dirs, files) in os.walk(pathin)
            for file in files]

class Server():
	"""responsible for managing clients, handling them requests and sending data from daemon to them"""
	def __init__(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("8.8.8.8", 80))
		self.host = s.getsockname()[0]
		s.close()		
		self.clientConnections = []
		self.daemonAddress = (self.host, 6001)
		serverAddress = (self.host, 6003)
		requestServiceAddress = (self.host, 6004)
		self.captureDataFlag = True
		try:
			self.daemonConn = Client(self.daemonAddress, authkey=b'somekey')
			self.delay = self.daemonConn.recv()
		except ConnectionRefusedError as e:
			print("Unable to connect to daemon, only file service available")
			self.captureDataFlag = False
			self.delay = None
		self.newClientListener = Listener(serverAddress, authkey = b'somekey')
		self.requestListener = Listener(requestServiceAddress, authkey = b'somekey')
		self.filesPath = os.path.dirname(os.path.abspath(__file__))+ '/../files/'

	def start(self):
		Thread(target = self.fileService).start()
		if self.captureDataFlag == True:
			Thread(target = self.captureNewData).start()
		Thread(target = self.acceptNewClient).start()
		

	def acceptNewClient(self):
		"""constantly waits for client connection and append it to stored client connection lists""" 
		while True:
			clientConn = self.newClientListener.accept()
			clientConn.send(self.delay)
			print("New client connection")
			self.clientConnections.append(clientConn)

	def fileService(self):
		"""waits for client file service connection and starts listening for request from him"""
		print("Wait for client connection")
		while True:
			clientConn = self.requestListener.accept()
			print("Client connected to file service")
			Thread(target = self.listenForRequest, args=(clientConn,)).start()

	def processRequestFromClient(self, request, clientConn):
		"""executes request from client"""
		requestDict = json.loads(request)
		if requestDict["command"] == "list_files":
			self.sendFileList(clientConn)
		elif requestDict["command"] == "send_file":
			self.sendFile(requestDict["payload"], clientConn)
		elif requestDict["command"] == "send_command":
			self.daemonConn.send(requestDict["payload"])
			clientConn.send("OK")

	def sendFileList(self, clientConn):
		"""sends file list to client"""
		fileList = unix_find(self.filesPath)
		toCut = self.filesPath + 'logFile.'
		cutFileList = [file.replace(toCut, '') for file in fileList if file.startswith(self.filesPath + 'logFile.')]
		clientConn.send(cutFileList)

	def listenForRequest(self, clientConn):
		"""listens for client requests and redirects it to processRequestFromClient method"""
		request = clientConn.recv()
		self.processRequestFromClient(request, clientConn)

	def sendFile(self, requestFile, clientConn):
		"""sends file to client"""
		path = self.filesPath + requestFile
		try:
			with open(path) as file:
				fileContent = file.readlines()
				clientConn.send(fileContent)
		except Exception as e:
			print(e)
		finally:
			clientConn.close()

	def tryReconnectToDaemon(self):
		"""every 15 seconds tries to reconnect to daemon"""
		while True:
			try:
				self.daemonConn = Client(self.daemonAddress, authkey=b'somekey')
				self.delay = self.daemonConn.recv()
				print("Connection to daemon established")
				Thread(target = self.captureNewData).start()
				break
			except ConnectionRefusedError as e:
				time.sleep(15)

	def captureNewData(self):
		"""gets new record from daemon and sends it to all clients"""
		while True:
			try:
				data = self.daemonConn.recv()
				self.sendToClients(data)
			except EOFError:
				print("Connection to daemon lost, only file service available")
				self.captureDataFlag = False
				self.delay = None
				self.tryReconnectToDaemon()
				break


	def sendToClients(self, data):
		"""sends data record to all clients"""
		for clientConn in self.clientConnections:
			try:
				clientConn.send(data)
			except IOError as e:
				print("Client disconnecting...")
				self.clientConnections.remove(clientConn)

if __name__ == '__main__':
	Server().start()




