import sys, os
import logging
from logging.handlers import *
from multiprocessing.connection import Listener
from threading import Thread
import argparse
import socket



#custom modules
import dataprocessing
from serialcontroller import SerialController


class MyRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
	"""responsible for rotating disk on files"""
	def _open(self):
		prevumask=os.umask(744)
		rtv=logging.handlers.TimedRotatingFileHandler._open(self)
		os.umask(prevumask)
		return rtv

class DaemonApp():
	def __init__(self):
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(("8.8.8.8", 80))
		self.host = s.getsockname()[0]
		s.close()
		parser = argparse.ArgumentParser()
		parser.add_argument("-d", "--delay",type=float, help="specifies delay with which data will be read from serial port")
		parser.add_argument("groupingUnit",choices =['M','H','D'], help="specifies unit with which file will be rotated on disk")
		args = parser.parse_args()
		if args.delay != None:
			self.delay = args.delay
		else:
			self.delay = 1
		log_format = "%(message)s"
		formatter = logging.Formatter(log_format)
		unit = args.groupingUnit
		handler = MyRotatingFileHandler(os.path.dirname(os.path.abspath(__file__)) + '/../files/logFile',
			when=unit,interval=1, delay = True)
		handler.setFormatter(formatter)
		suffix = {
			'M'  : "%Y-%m-%d_%H:%M",
			'H'  : "%Y-%m-%d_%H",
			'D'  : "%Y-%m-%d",
		}
		handler.suffix = suffix[unit]
		handler.extMatch = re.compile(r"^.{10,19}$")

		logger = logging.getLogger("myLogger")
		logger.addHandler(handler)
		logger.setLevel(logging.INFO)

		self.connectionLessDataProcessor = dataprocessing.ConnectionLessDataProcessor(logger, handler, unit)
		self.connectionDataProcessor = dataprocessing.ConnectionDataProcessor(logger, handler, unit)
		self.dataProcessor = self.connectionLessDataProcessor

	def waitForServerConnection(self):
		"""waits for server connection, when server is connected then changes data procesor to ConnectionDataProcessor"""
		address = (self.host, 6001)
		listener = Listener(address, authkey=b'somekey')
		print("wait for server connection accepting: ...")
		self.conn = listener.accept()
		self.conn.send(self.delay)
		print("server connection accepted")
		self.setDataProcessor(self.connectionDataProcessor)
		self.dataProcessor.setConnection(self.conn)
		thread = Thread(target = self.listen)
		thread.start()

	def setDataProcessor(self,dataProcessor):
		"""sets data processor

		:param: dataProcessor
		:type: DataProcessor"""
		self.dataProcessor = dataProcessor
		self.srl.setDataProcessor(dataProcessor)

	def listen(self):
		"""listens for message from server if server sends message that cause exception that data processor is changed to ConnectionLessDataProcessor"""
		try:
			msg = self.conn.recv()
			self.srl.sendCommandToSerial(msg)
			self.listen()
		except EOFError as e:
			self.conn.close()
			self.setDataProcessor(self.connectionLessDataProcessor)
			thread = Thread(target = self.waitForServerConnection)
			thread.start()
		except Exception as e:
			print("Unexpected exception %s", e)
			self.setDataProcessor(self.connectionLessDataProcessor)
			thread = Thread(target = self.waitForServerConnection)
			thread.start()			


	def main(self):
		"""starts new thread in order to connect with server and starts to read data from serial port invoking SerialInputController.start method for this purpose"""
		thread = Thread(target = self.waitForServerConnection)
		thread.start()
		self.srl = SerialController(self.dataProcessor, self.delay)
		self.srl.start()

if __name__ == "__main__":
	daemonApplication = DaemonApp()
	daemonApplication.main()



	
