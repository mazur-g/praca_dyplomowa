import numpy as np
from datetime import datetime
import matplotlib.dates as mdates
import sys

now = datetime.now()
num_now = mdates.date2num(now)

def format(value):
	return "%.10f" % value

def to_date(number):
	mdate = mdates.num2date(num_now + number/25)
	return mdate.strftime("%Y-%m-%d %H:%M:%S")

def prepareValues(valueTuple, withDate=True):
	to_save = []
	if withDate:
		to_save.append(valueTuple[0])
	for value in valueTuple[1:]:
		if np.isnan(value):
			to_save.append('NaN')
		else:
			to_save.append(format(value))
	return to_save

x_start = -10
x_end = 10
x_diff = 0.007

x = np.arange(x_start, x_end, x_diff)

y_values = []
y_sin = np.sin(x)
y_exp = np.exp(x)
y_arccos = np.arccos(x)
y_tanh = np.tanh(x)

x = list(map(to_date, x))
zippedValues = list(zip(x,y_sin, y_exp, y_arccos, y_tanh))

f_pos = open('../files/logFile.testData-positional', 'w')
f_keyValue = open('../files/logFile.testData-keyValue', 'w')

for valueTuple in zippedValues[:-1]:
	to_save_pos = prepareValues(valueTuple)
	to_save_keyValue = prepareValues(valueTuple, withDate=False)
	f_pos.write('{}\t{}\t{}\t{}\t{}\n'.format(*to_save_pos))
	f_keyValue.write('sinus={}\texp={}\tarccos={}\ttanh={}\n'.format(*to_save_keyValue))
lastValueTuple = zippedValues[-1]
to_save_pos = prepareValues(lastValueTuple)
to_save_keyValue = prepareValues(lastValueTuple, withDate=False)
f_pos.write('{}\t{}\t{}\t{}\t{}'.format(*to_save_pos))
f_keyValue.write('sinus={}\texp={}\tarccos={}\ttanh={}'.format(*to_save_keyValue))	

f_pos.close()
f_keyValue.close()