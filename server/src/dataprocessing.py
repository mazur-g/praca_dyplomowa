from abc import ABCMeta, abstractmethod
import datetime
from multiprocessing.connection import Client
from multiprocessing.connection import Listener
import math
import re

class DataProcessor():
	"""abstract class for data processing, encapsulates such methods as createMessage and decideAboutRollover"""
	__metaclass__ = ABCMeta
	@abstractmethod
	def processFirstPeriod(self, Input):
		pass
	@abstractmethod
	def processFurtherPeriods(self, Input):
		pass

	def __init__(self, rotatingUnit):
		self.previous = -1
		self.rotatingUnit = rotatingUnit
		self.leftIndex = None
		self.rightIndex = None
		if rotatingUnit == 'M':
			self.leftIndex = 17
			self.rightIndex = 19
		elif rotatingUnit == 'H':
			self.leftIndex = 14
			self.rightIndex = 16
		elif rotatingUnit == 'D':
			self.leftIndex = 11
			self.rightIndex = 13


	def createMessage(self, Input):
		"""prepares message that is ready to send to server"""
		regex = r"[0-9]{4}-(?:0|1)[0-9]-(?:0|1|2|3)[0-9] [0-9]{2}:[0-9]{2}:[0-9]{2}(?:\.[0-9]+)?"
		pattern = re.compile(regex)
		message = Input.decode("utf-8").rstrip()
		match = pattern.match(message[:19])
		if match:
			return message
		else:
			date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")
			return (date + '\t' + message)

	def decideAboutRollover(self, message):
		"""decide if it is time to rotate file on disks to have data aggregated nicely for example from 00:01 to 23:59 instead of 13:43 to 12:31 based on message date part"""
		presentIndicator = int(message[self.leftIndex:self.rightIndex])
		if presentIndicator == 0 or presentIndicator < self.previous:
			try:
				self.handler.doRollover()
				self.process = self.processFurtherPeriods
			except Exception as e:
				print(e)
		self.previous = presentIndicator



class ConnectionLessDataProcessor(DataProcessor):
	"""responsible for processing data without connection to server, only saves records to file"""
	def __init__(self, logger, handler, rotatingUnit):
		super(ConnectionLessDataProcessor, self).__init__(rotatingUnit)
		self.logger = logger
		self.handler = handler
		self.process = self.processFirstPeriod

	def processFirstPeriod(self, Input):
		"""process first period when the file was not yet rotated, saves records to file"""
		message = self.createMessage(Input)
		self.decideAboutRollover(message)
		self.logger.info(message)

	def processFurtherPeriods(self, Input):
		"""process further periods when the file is already rotated once or more times
		, now it is able to count one period for example 1 day and then rotate the file, saves records to file""" 
		message = self.createMessage(Input)
		self.logger.info(message)


class ConnectionDataProcessor(DataProcessor):
	"""responsible for processing data with connection to server, saves records to file and sends records to server"""
	def __init__(self, logger, handler, rotatingUnit):
		super(ConnectionDataProcessor, self).__init__(rotatingUnit)
		self.logger = logger
		self.handler = handler
		self.process = self.processFirstPeriod

	def processFirstPeriod(self, Input):
		"""process first period when the file was not yet rotated, saves records to file and sends them to server"""
		message = self.createMessage(Input)
		self.decideAboutRollover(message)
		self.logger.info(message)
		try:
			self.conn.send(message)
		except Exception as e:
			print(e)

	def processFurtherPeriods(self, Input):
		"""process further periods wphen the file is already rotated once or more times, now it is able to count one period for example 1 day and then rotate the fil, saves records to file and send them to server"""
		message = self.createMessage(Input)
		self.logger.info(message)
		try:
			self.conn.send(message)
		except Exception as e:
			print(e)

	def setConnection(self,connection):
		self.conn = connection