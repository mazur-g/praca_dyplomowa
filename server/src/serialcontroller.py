import random
import serial
import time
from daemonize import Daemonize

class SerialController(object, ):
	"""responsible for taking operations on serial port"""
	def __init__(self, dataProcessor, delay):
		self.delay = delay
		self.dataProcessor = dataProcessor
		self.pid = "daemon.pid"
		with open('../config/serialPort', 'r') as f:
			self.port = f.readline().rstrip()

	def setDelay(self, delay):
		"""sets delay with which data will be read from serial port"""
		self.delay = delay

	def setDataProcessor(self,dataProcessor):
		"""sets data processor which will be used to process data read from serial port"""
		self.dataProcessor = dataProcessor

	def start(self):
		"""daemonizes the process and calls to processInputFromSerial method"""
		daemon = Daemonize(app="test_app", pid = self.pid, action = self.processInputFromSerial, auto_close_fds = False, foreground = True)
		daemon.start()

	def processInputFromSerial(self):
		"""reads data from serial port with set delay and passes them to dataProcessor.process method"""
		time.sleep(2)
		try:
			with serial.Serial(self.port) as ser:
				while True:
					line = ser.readline()
					self.dataProcessor.process(line)
		except Exception as e:
			print(e)

	def sendCommandToSerial(self, command):
		with serial.Serial(self.port) as ser:
			ser.write(command.encode())
